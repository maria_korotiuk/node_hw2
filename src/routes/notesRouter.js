const router = require('express').Router();
const notesController = require('./../controllers/notesController');

router.get('/', notesController.getNotes);
router.post('/', notesController.createNote);
router.get('/:id', notesController.getNote);
router.put('/:id', notesController.updateNote);
router.patch('/:id', notesController.checkNote);
router.delete('/:id', notesController.deleteNote);

module.exports = router;