const router = require('express').Router();
const authRouter = require('./authRouter');
const notesRouter = require('./notesRouter');
const usersRouter = require('./usersRouter');
const {authMiddleware} = require('./../middelware/authMiddleware');

router.use('/auth', authRouter);
router.use('/notes', authMiddleware, notesRouter);
router.use('/users', authMiddleware, usersRouter);

module.exports = router;