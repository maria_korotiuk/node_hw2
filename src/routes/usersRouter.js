const router = require('express').Router();
const usersController = require('./../controllers/usersController');
const {validatePassword} = require('./../middelware/validationMiddleware');

router.get('/me', usersController.getUser);
router.delete('/me', usersController.deleteUser);
router.patch('/me', validatePassword, usersController.updateUser);

module.exports = router;