const joi = require('joi');

const registrationValidator = async (req, res, next) => {
  const schema = joi.object({
    username: joi.string()
      .alphanum()
      .min(3)
      .required(),
    password: joi.string()
      .min(3)
      .required()
  });
  try {
    await schema.validateAsync(req.body);
    next();
  }
  catch (error) {
    res.status(400).json({
      "message": error.details[0].message
    });
  }
}

const validatePassword = async (req, res, next) => {
  console.log('BBBB');
  const schema = joi.object({
    password: joi.string()
      .min(3)
      .required()
  });
  try {
    await schema.validateAsync({password: req.body.newPassword});
    next();
  } catch (error) {
    res.status(400).json({
      "message": error.details[0].message
    });
  }
}

module.exports = { 
  registrationValidator,
  validatePassword
};