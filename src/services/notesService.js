const Note = require('./../models/noteModel');

const getNotes = async (userId, limit, offset) => {
  console.log(limit);
  console.log(offset);
  const notes = await Note.find({userId}).limit(limit).skip(offset);
  return notes;
}

const createNote = async (userId, text) => {
  const note = new Note({
    userId,
    text
  });
  await note.save();
}

const getNote = async (userId, noteId) => {
  const note = await Note.findOne({_id: noteId, userId: userId});
  return note;
}

const updateNote = async (userId, noteId, text) => {
  await Note.findOneAndUpdate({_id: noteId, userId: userId}, {text: text})
}

const checkNote = async (userId, noteId) => {
  const completed = await Note.findOne({_id: noteId, userId: userId}).completed;
  if (completed) {
    await Note.findOneAndUpdate({_id: noteId, userId: userId}, {completed: false})
  } else {
    await Note.findOneAndUpdate({_id: noteId, userId: userId}, {completed: true})
  }
}

const deleteNote = async (userId, noteId) => {
  const note = await Note.findOne({ _id: noteId, userId: userId });
  await Note.findOneAndRemove({_id: noteId, userId: userId});
}

module.exports = {
  getNotes,
  createNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote
};