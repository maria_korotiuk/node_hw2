const User = require('./../models/userModel');
const Credentials = require('./../models/credentialsModel');
const bcrypt = require('bcrypt');


const getUser = async (username) => {
  const user = await User.findOne({ username });
  return user;
}

const deleteUser = async (username) => {
  const user = await User.findOne({ username });
  if (user) {
    await User.findOneAndRemove({username});
    await Credentials.findOneAndRemove({username});
  } else {
    throw new Error('User doesn\'t exist');
  }
}

const changePassword = async (username, oldPassword, newPassword) => {
  const oldCredentials = await Credentials.findOne({username});
  if (!(await bcrypt.compare(oldPassword, oldCredentials.password))) {
    throw new Error('Invalid old password');
  } else {
    const user = await Credentials.findOneAndUpdate({username}, {password: await bcrypt.hash(newPassword, 10)});
  }
}

module.exports = {
  getUser,
  deleteUser,
  changePassword
}